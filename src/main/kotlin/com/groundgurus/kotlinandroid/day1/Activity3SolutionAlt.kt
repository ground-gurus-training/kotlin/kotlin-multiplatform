package com.groundgurus.kotlinandroid.day1

import java.time.LocalDate

fun main() {
  var year = LocalDate.now().year + 1
  var count = 0

  println("The leap years are: ")
  while (count != 20) {
    if (isLeapYear(year)) {
      println("${count + 1}. $year")
      count++
    }

    year++
  }
}

fun isLeapYearAlt(year: Int): Boolean {
  return if (year % 4 == 0) {
    if (year % 100 == 0) {
      // year is divisible by 400,
      // hence the year is a leap year
      year % 400 == 0
    } else {
      true
    }
  } else {
    false
  }
}
