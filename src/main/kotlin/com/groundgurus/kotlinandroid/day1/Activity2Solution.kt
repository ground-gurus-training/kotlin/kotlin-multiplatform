package com.groundgurus.kotlinandroid.day1

fun main() {
  val numbers = listOf(10, 11, 12, 13)
  println("Odd = ${getOdds(numbers)}")
  println("Even = ${getEvens(numbers)}")
}

fun getOdds(numbers: List<Int>): List<Int> {
  val toReturn = mutableListOf<Int>()
  for (num in numbers) {
    if (num % 2 != 0) {
      toReturn += num
    }
  }
  return toReturn
}

fun getEvens(numbers: List<Int>): List<Int> {
  val toReturn = mutableListOf<Int>()
  for (num in numbers) {
    if (num % 2 == 0) {
      toReturn += num
    }
  }
  return toReturn
}
