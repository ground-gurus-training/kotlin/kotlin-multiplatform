package com.groundgurus.kotlinandroid.day1

fun main() {
  print("Enter your First Name: ")
  val firstName = readLine()

  print("Enter your Last Name: ")
  val lastName = readLine()

  println("Hello $firstName $lastName!")
}
