package com.groundgurus.kotlinandroid.day1

open class Employee2(val firstName: String, val lastName: String, val department: String) {
  open fun printDetails() {
    println("Employee Name: $firstName $lastName")
    println("Department: $department")
  }
}

class Programmer(firstName: String, lastName: String, department: String, val skills: List<String>)
  : Employee2(firstName, lastName, department) {
  override fun printDetails() {
    super.printDetails()
    println("Skills: ")
    for (skill in skills) {
      println("- $skill")
    }
  }
}

fun main() {
  val programmer1 = Programmer("Sam", "Winchester", "Development",
    listOf("Kotlin", "Web Design", "MongoDB"))
  programmer1.printDetails()
}
