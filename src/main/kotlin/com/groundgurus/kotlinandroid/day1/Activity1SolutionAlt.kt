package com.groundgurus.kotlinandroid.day1

fun main() {
  val firstName = readText("Enter your First Name: ")
  val lastName = readText("Enter your Last Name: ")

  println("Hello $firstName $lastName!")
}

fun readText(prompt: String): String? {
  print(prompt)
  return readLine()
}
